$(document).ready(function() {
    var production_slider = new Swiper('#production_slider', {
            slidesPerView: 3,
            spaceBetween: 30,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            loop: true,
            slidesPerGroup: 3,
            loopFillGroupWithBlank: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                1199: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
            },
        });


    var mainpage_about_slider = new Swiper('#mainpage_about_slider', {
            spaceBetween: 5,
            centeredSlides: true,
            effect: 'fade',
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });

    
    var about_us_page_slider = new Swiper('#about_us_page_slider', {
            centeredSlides: true,
            effect: 'fade',
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            loop: true,
        });


    var portfolio_slider = new Swiper('#portfolio_slider', {
            slidesPerView: 3,
            slidesPerColumn: 2,
            spaceBetween: 20,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination_mainpage-portfolio',
                clickable: true
            },
            breakpoints: {
                767: {
                    slidesPerView: 6,
                    slidesPerColumn: 6,
                    autoplay: false,
                    pagination: false,
                },
                1199: {
                    slidesPerView: 2,
                    slidesPerColumn: 3,
                },
            },
        });


    var mainpage_news_slider = new Swiper('#mainpage_news_slider', {
            slidesPerView: 3,
            spaceBetween: 30,
            slidesPerGroup: 3,
            loop: true,
            loopFillGroupWithBlank: true,
            pagination: {
                el: '.swiper-pagination_mainpage-news',
                clickable: true
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                1199: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
            },
        });


    var customers_slider = new Swiper('#customers_slider', {
            slidesPerView: 5,
            spaceBetween: 16,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            slidesPerGroup: 5,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '#customers_slider + .block-customers-slider__arrows .swiper-button-next',
                prevEl: '#customers_slider + .block-customers-slider__arrows .swiper-button-prev',
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                1199: {
                    slidesPerView: 3,
                    slidesPerGroup: 3,
                },
            },
        });


    var portfolio_page_documents_slider = new Swiper('#portfolio_page_documents_slider', {
            slidesPerView: 4,
            spaceBetween: 30,
            slidesPerGroup: 4,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                1199: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
            },
        });


    var product_page_documents_slider = new Swiper('#product_page_documents_slider', {
            slidesPerView: 4,
            spaceBetween: 30,
            slidesPerGroup: 4,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                1199: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
            },
        });


    var documents_slider = new Swiper('#documents_slider', {
            slidesPerView: 4,
            spaceBetween: 30,
            slidesPerGroup: 4,
            loop: true,
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '#documents_slider + .block-dark-slider__arrows .swiper-button-next',
                prevEl: '#documents_slider + .block-dark-slider__arrows .swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpoints: {
                767: {
                    slidesPerView: 1,
                    slidesPerGroup: 1,
                },
                1199: {
                    slidesPerView: 2,
                    slidesPerGroup: 2,
                },
            },
        });

    
    var company_history_years_slider = new Swiper('#company_history_years_slider', {
            slidesPerView: 3,
            spaceBetween: 10,
            loop: true,
            navigation: {
                nextEl: '#company_history_years_slider + .company-history-years-slider__arrows .swiper-button-next',
                prevEl: '#company_history_years_slider + .company-history-years-slider__arrows .swiper-button-prev',
            },
            slideToClickedSlide: true,
            centeredSlides: true,
            on: {
                slideChange: function() {
                    console.log( this.realIndex, $('#company_history_years_slider').find('[data-swiper-slide-index="' + this.realIndex + '"] label').text() );
                    $('#company_history_years_slider').find('[data-swiper-slide-index="' + this.realIndex + '"] [name="history_years"]').prop("checked", true).trigger('change');
                }
            }
        });


    var product_page_slider = new Swiper('#product_page_slider', {
            slidesPerView: 1,
            spaceBetween: 10,
            loop: true,
            navigation: {
                nextEl: '#product_page_slider + .slider__arrows .swiper-button-next',
                prevEl: '#product_page_slider + .slider__arrows .swiper-button-prev',
            },
            thumbs: {
                swiper: new Swiper('#product_page_slider_thumbs', {
                    slidesPerView: 4,
                    freeMode: true,
                    watchSlidesVisibility: true,
                    watchSlidesProgress: true,
                    breakpoints: {
                        767: {
                            slidesPerView: 2,
                            spaceBetween: 0,
                        }
                    },
                }),
            },
        });
    
    
    $('.company-history-event:first-child [id^="company_history_slider_"]').each(function() {
        var slider_id = $(this).data('history-slider-id'),
            selector = '#' + $(this).attr('id'),
            company_history_slider = new Swiper(selector, {
                slidesPerView: 1,
                loop: true,
                navigation: {
                    nextEl: $(selector).parent().next('.slider__arrows').find('.swiper-button-next'),
                    prevEl: $(selector).parent().next('.slider__arrows').find('.swiper-button-prev'),
                },
                thumbs: {
                    swiper: new Swiper('#company_history_slider_thumbs_' + slider_id, {
                        slidesPerView: 4,
                        freeMode: true,
                        watchSlidesVisibility: true,
                        watchSlidesProgress: true,
                        breakpoints: {
                            767: {
                                slidesPerView: 2,
                            },
                        },
                    }),
                },
            });

        console.log( $(selector).parent().next() );
    });


    /* первый слайдер на главной странице */
    var mainpage_slider = new Swiper('#mainpage_slider', {
        effect: 'fade',
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        loop: true,
        pagination: {
            el: '.swiper-pagination_mainpage-slider',
            clickable: true
        }
    });
});