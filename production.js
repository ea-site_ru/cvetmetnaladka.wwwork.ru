$(document).ready(function(){
    // перемотка к вкладке
    $(document).on('click', '[data-switch-to-tab]', function(event) {
        event.preventDefault();

        var tab_name = $(this).data('switch-to-tab');

        $('[name="product_page_tabs_label"][value="' + tab_name + '"]').attr('checked', true).trigger('change');

        if ( $(document).width() >= 768 ) {
            $("html, body").animate({
                scrollTop: ( $('.block_product-page-tabs').offset().top - parseFloat( $('.block_product-page-tabs').css('margin-top') ) - $('.header').outerHeight() ) + "px"
            });
        } else {
            $("html, body").animate({
                scrollTop: ( $('[id="product_page_tab_' + tab_name + '"]').parent().prev('LABEL').offset().top - $('.header').outerHeight() ) + "px"
            });
        }
    });
    // \\ перемотка к вкладке


    // переключение вкладок на странице продукта
    $(document).on('change', '[name="product_page_tabs_label"]', function(event) {
        event.preventDefault();

        var tab_name = $(this).val();

        if ( $(document).width() >= 768 ) {
            $('[name="product_page_tabs_label"]').parent('LABEL').removeClass('active');
            $(this).parent('LABEL').addClass('active');

            $('[id^="product_page_tab_"]:not(.d-none)').addClass('d-none');
            $('[id="product_page_tab_' + tab_name + '"]').removeClass('d-none');
        } else {
            if ( !$(this).parent('LABEL').hasClass('active') )
                $(this).parent('LABEL').addClass('active');
            else
                $(this).parent('LABEL').removeClass('active');

            if ( !$('[id="product_page_tab_' + tab_name + '"]').hasClass('d-none') )
                $('[id="product_page_tab_' + tab_name + '"]').addClass('d-none');
            else
                $('[id="product_page_tab_' + tab_name + '"]').removeClass('d-none');
        }
    });
    // \\ переключение вкладок на странице продукта
});