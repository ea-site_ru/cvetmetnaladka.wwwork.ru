/* безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */
$(document).on('click', '[target="_blank"]', function(e) {
    e.preventDefault();

    var otherWindow = window.open();
    otherWindow.opener = null;
    otherWindow.location = $(this).attr('href');
});
/* \\ безопасный target="_blank" (см. https://habr.com/ru/post/282880/) */


// если страница при загрузке прокручена - добавляем шапке класс scrolling
if ( $(window).scrollTop() > 0 )
    $('.header').addClass('scrolling');
// \\ если страница при загрузке прокручена - добавляем шапке класс scrolling


$(document).ready(function() {
    // скрываем прелоадер
    $('#preloader').fadeOut(500);

    // добавляем верхний отступ для контента, чтобы он не заходил под шапку сайта
    $('HTML[data-device!="desktop"] BODY > main, HTML[data-device="desktop"] BODY:not(.mainpage) > main').css('padding-top', $('.header').outerHeight() + 'px');

    // адаптивные таблицы для смартфонов
    if ( $(document).width() < 768 ) {
        $('table').each(function() {
            $(this).wrap('<div class="table-responsive-wrap"></table>');
        });
    }
    // \\ адаптивные таблицы для смартфонов

    // сворачивание/разворачивание блоков
    $(document).on('click', '[data-toggle]', function(e, closeMenu) {
        e.preventDefault();

        if ( $(this).attr('data-toggled-class') )
            var toggled_class = $(this).data('toggled-class');
        else
            var toggled_class = 'd-none';

        if ( $(this).data('toggle') == '#menu' ) {
            $('.header__menu-panel').toggleClass('header__menu-panel_bordered');

            // убираем прокрутку страницы при открытии меню
            if ( !$('#menu').hasClass( $(this).data('toggled-class') ) ) {
                $('BODY').css({
                    "position":"fixed",
                    "width":"100%",
                    "overflow-y":"scroll"
                });
            } else {
                $('BODY').removeAttr('style');
            }

            // скрываем меню при клике вне его
            if ( closeMenu === true ) {
                $('#menu').removeClass( $(this).data('toggled-class') );
                $('BODY').removeAttr('style');
                return;
            }
        }

        $( $(this).data('toggle') ).toggleClass(toggled_class);

        if ( $(this).data('toggle-class-targets') )
            $( $(this).data('toggle-class-targets') ).each(function() {
                $(this).toggleClass('active');
            });

        $(this).toggleClass('active');
    });

    // скрытие меню при клике вне его
    $(document).on('click', '*:not([data-toggle="#menu"])', function(e) {
        if (
            $('#menu').hasClass('visible') &&
            $('#menu').has(e.target).length === 0 &&
            $('#menu').siblings('.header__menu-panel').has(e.target).length === 0 &&
            !e.target.className.match(/header__menu-panel|header__menu-wrap|btn_menu/)
        ) {
            e.stopPropagation();
            $('[data-toggle="#menu"]').trigger('click', [true]);
        }
    });
    // \\ скрытие меню при клике вне его

    // проверка ввода поискового запроса перед отправкой формы поиска
    $(document).on('submit', 'form[name="search_form"]', function(event) {
        var query_field = $(this).find('[name="query"]'),
            default_placeholder = query_field.attr('placeholder');

        if ( !query_field.val() ) {
            query_field.attr('placeholder', 'Введите поисковый запрос!');

            setTimeout(function() {
                query_field.attr('placeholder', default_placeholder).focus();
            }, 2500);

            event.preventDefault();
        }
    });
    // \\ проверка ввода поискового запроса перед отправкой формы поиска

    // прокрутка страницы
    var footer_top_offset = $(document).height() - $(window).height();

    if ( $(window).scrollTop() >= footer_top_offset && !$('[data-scroll-page]').hasClass('flip') )
        $('[data-scroll-page]').addClass('flip');

    $(document).on('click', '[data-scroll-page]', function(e) {
        if ( $(window).scrollTop() < footer_top_offset )
            $('html, body').animate({ scrollTop: footer_top_offset }, 1000);
        else
            $('html, body').animate({ scrollTop: 0 }, 1000);
    });

    $(document).on('click', '[data-scroll-to-office]', function(e) {
        var target = $( $(this).data('scroll-to-office') );

        $('[data-scroll-to-office]').removeClass('active');
        $(this).addClass('active');

        $('.contacts-tile').parent('.tile-wrap_contacts-page').removeClass('selected');
        target.next('.contacts-tile').parent('.tile-wrap_contacts-page').addClass('selected');

        $('html, body').animate({ scrollTop: target.offset().top - $('header.header').outerHeight() }, 1000);
    });

    // анимированные placeholder'ы
    $('[placeholder]:not(.input_guard):not(.input_search)').each(function() {
        var placeholder = $(this).attr('placeholder');

        $(this).wrap('<div class="input-custom-placeholder-wrap"></div>')
               .after('<div class="input-custom-placeholder">' + placeholder + '</div>')
               .attr('placeholder', '');
    });

    $('[placeholder]:not(.input_guard):not(.input_search)').on('input blur', function() {
        if ( $(this).val() != '' && !$(this).hasClass('filled') )
            $(this).addClass('filled');
        else if ( $(this).val() == '' )
            $(this).removeClass('filled');
    });

    $('[name="phone"]').on('blur', function() {
        if ( $(this).val() == '+7(___)___-__-__' )
            $(this).val('').removeClass('filled');
    });
    // \\ анимированные placeholder'ы

    // ajax загрузка данных офиса
    $(document).on('click', '[data-load-office-info]', function(e) {
        e.preventDefault();

        var self = $(this),
            officeId = self.data('load-office-info');

        $.ajax({
            type: 'POST',
            data: {
                action: 'getOfficeInfo',
                officeId: officeId,
                showOfficeIcon: 1,
                showOfficeName: 1
            },
            success: function(res) {
                $('#mainpage_office_info').html(res);
                $('[data-load-office-info]').removeClass('active');
                self.addClass('active');
            }
        });
    });
    // \\ ajax загрузка данных офиса

    // ajax запросы при нажатии на кнопки с [data-ajax]
    $(document).on('click', '[data-ajax]', function(e) {
        e.preventDefault();

        var self = $(this),
            action = self.data('ajax'),
            snippet_params = $.parseJSON( self.data('params').replace(/'/g, '"') ),
            params = {},
            reload = self.data('ajax-reload'),
            form  = ( $('#' + self.attr('form')).length > 0 ) ? $('#' + self.attr('form').get(0)) : // если у элемента с атрибутом data-ajax есть атрибут form получаем связанную с ним форму
                    ( self.parent('form').length > 0 ? self.parent('form').get(0) : // если элемент лежит непосредственно в форме
                    ( self.parentsUntil('form').length > 0 && self.parentsUntil('form')[self.parentsUntil('form').length - 1].tagName != 'HTML' ) ? self.parentsUntil('form').parent().get(0) : null ); // если элемент лежит на любом уровне вложенности в форме

        // собираем данные с формы
        if ( form ) {
            params = new FormData( form );
            params.append( 'action', action );
            for (key in snippet_params)
                params.append( key, decodeURIComponent(snippet_params[key]) );

            // если найдена форма
            $.ajaxSetup({
                contentType: false, // важно - убираем форматирование данных по умолчанию
                processData: false, // важно - убираем преобразование строк по умолчанию
            });
        } else { // или из атрибута data-params
            params['action'] = action;
            for (key in snippet_params)
                params[key] = decodeURIComponent(snippet_params[key]);

            // пустые настройки по-умолчанию
            $.ajaxSetup({});
        }


        // отправляем запрос
        $.ajax({
            dataType: 'json', // тип ожидаемых данных в ответе
            type: 'POST',
            data: params,
            success: function() {
                // очищаем статусы полей
                if ( form )
                    form.find('.input.success, .input.error').removeClass('success error');
            }
        });
    });
    // \\ ajax запросы при нажатии на кнопки с [data-ajax]

    // переключение событий в блоке "История компании" на странице "О нас"
    $(document).on('change', '[name="history_years"]', function() {
        var event_idx = $(this).val();

        $('[data-history-event]:not(.d-none)').addClass('d-none');
        $('[data-history-event=' + event_idx + ']').removeClass('d-none');


        $('[id="company_history_slider_' + $(this).val() + '"]').each(function() {
            var slider_id = $(this).data('history-slider-id'),
                selector = '#' + $(this).attr('id'),
                company_history_slider = new Swiper(selector, {
                    slidesPerView: 1,
                    loop: true,
                    navigation: {
                        nextEl: $(selector).parent().next('.slider__arrows').find('.swiper-button-next'),
                        prevEl: $(selector).parent().next('.slider__arrows').find('.swiper-button-prev'),
                    },
                    thumbs: {
                        swiper: new Swiper('#company_history_slider_thumbs_' + slider_id, {
                            slidesPerView: 4,
                            freeMode: true,
                            watchSlidesVisibility: true,
                            watchSlidesProgress: true,
                            breakpoints: {
                                767: {
                                    slidesPerView: 2,
                                },
                            },
                        }),
                    },
                });
        });
    })
    // \\ переключение событий в блоке "История компании" на странице "О нас"

    // маска для ввода номера телефона
    $('[name="phone"]').mask("+7(999)999-99-99");

    // POPUPS
    $.extend(true, $.magnificPopup.defaults, {
        tClose: 'Закрыть (Esc)', // Alt text on close button
        tLoading: 'Загрузка...', // Text that is displayed during loading. Can contain %curr% and %total% keys
        gallery: {
            tPrev: 'Предыдущий (Стрелка влево)', // Alt text on left arrow
            tNext: 'Следующий (Стрелка вправо)', // Alt text on right arrow
            tCounter: '%curr% из %total%' // Markup for "1 of 7" counter
        },
        image: {
            tError: '<a href="%url%">Изображение</a> не может быть загружено.' // Error message when image could not be loaded
        },
        ajax: {
            tError: '<a href="%url%">Содержимое</a> не может быть загружено.' // Error message when ajax request failed
        },
        closeMarkup: '<button title="%title%" type="button" class="mfp-close"></button>',

        callbacks: {
            open: function() {
                $('HTML').css('overflow', 'auto');
            }
        }
    });

    $('[data-popup]').each(function() {
        var self = $(this),
            popup_type = self.data('popup') ? self.data('popup') : 'inline';

        switch (popup_type) {
          case 'image':
            var $showCloseBtn = true;

            self.magnificPopup({
              delegate: '.tile.swiper-slide:not(.swiper-slide-duplicate) a, *:not(.swiper-slide) > a',
              type: 'image',
              tLoading: 'Загрузка изображения #%curr%...',
              mainClass: 'mfp-img-mobile',
              showCloseBtn: $showCloseBtn,
              image: {
                  tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.',
              },
              callbacks: {
                  open: function() {
                    createZoomBtn('.mfp-img', '.mfp-figure');
                  }
              }
            });
            break;

          case 'gallery':
            var $showCloseBtn = true;

            self.magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Загрузка изображения #%curr%...',
                mainClass: 'mfp-img-mobile',
                showCloseBtn: $showCloseBtn,
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                },
                image: {
                    tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.',
                },
                callbacks: {
                    buildControls: function() {
                        // re-appends controls inside the main container
                        this.arrowLeft.appendTo(this.contentContainer);
                        this.arrowRight.appendTo(this.contentContainer);
                    },
                    open: function() {
                      createZoomBtn('.mfp-img', '.mfp-figure');
                    },
                    imageLoadComplete: function() {
                        var newImgLink = $('.mfp-img').attr('src');
                        $('.zoomImg').attr('src', newImgLink);
                    }
                }
            })
            break;

          default:
            self.magnificPopup({
                type: popup_type,
                showCloseBtn: $showCloseBtn,
                closeBtnInside: true,
            });
            break;
        }

        function createZoomBtn(el, wrap) {
          var elParent = $(el).parent();
          var generalParent = $(el).parents(wrap);

          generalParent.prepend('<button class="btn-plus" data-zoom-plus></button>');
          generalParent.prepend('<button class="btn-minus" data-zoom-minus></button>');

          $(document).on('click', '[data-zoom-plus]', function(e) {
            e.preventDefault;
            elParent.zoom({
                magnify: 1.5
            });
          });

          $(document).on('click', '[data-zoom-minus]', function(e) {
            e.preventDefault;
            elParent.trigger('zoom.destroy');
          });
        }
    });
});

$(window).scroll(function() {
    if ( $(window).scrollTop() > 0 && !$('.header').hasClass('scrolling') )
        $('.header').addClass('scrolling');
    else if ( $(window).scrollTop() == 0 && !$('#menu').hasClass('visible') )
        $('.header').removeClass('scrolling');

    if ( !footer_top_offset )
        var footer_top_offset = $(document).height() - $(window).height();

    if ( $(window).scrollTop() >= footer_top_offset && !$('[data-scroll-page]').hasClass('flip') )
        $('[data-scroll-page]').addClass('flip');
    else if ( $(window).scrollTop() < footer_top_offset )
        $('[data-scroll-page]').removeClass('flip');
});


/* действия по успешной отправке формы через AjaxForm */
$(document).on('af_complete', function(event, response) {
    // автоматическое закрытие magnificpopup при успешной отправке формы
    if (response.success)
        $.magnificPopup.close();
});
/* \\ действия по успешной отправке формы через AjaxForm */


/* YANDEX MAPS */
(function maps() {

    var yandexMapsInit = function (c) {

        if (!$('[data-map').length) return false;


        $.getScript('https://api-maps.yandex.ru/2.1?apikey=' + $yandex_maps_api_key + '&lang=ru_RU', function () {

            ymaps.ready(init);

            function init() {
                $('[data-map]').each(function () {
                    var $t = $(this),
                        address = $t.data('map');

                    $t.html('');

                    ymaps.geocode(address, {
                        results: 1
                    }).then(function (res) {
                        var firstGeoObject = res.geoObjects.get(0),
                            coords = firstGeoObject.geometry.getCoordinates();


                        var placemark = new ymaps.Placemark(coords, {
                            balloonContent: firstGeoObject.properties._data.balloonContent
                        }, {
                            iconLayout: 'default#image',
                            iconImageHref: '/assets/images/map_marker.png',
                            iconImageSize: [63, 81],
                            iconImageOffset: [-31, -80],
                        });


                        var myMap = new ymaps.Map($t.attr('id'), {
                            center: coords,
                            zoom: 16,
                            controls: ['zoomControl', 'typeSelector']
                        });
                        myMap.behaviors.disable("scrollZoom");
                        myMap.geoObjects.add(placemark);
                    });
                });
            }
        });
    }

    yandexMapsInit($(this));
})();
